#!/bin/bash

# Copyright (c) 2020, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Usage:
# 1) The script is expecting a file (be it an email or just copied text) as
#    argument which can get grepped for potential fingerprints.
# 2) Make sure to run this script in the dirauth-conf git repo to be sure to
#    just have the .conf files in it grepped for fingerprints.
# 3) The script will output all not-yet-rejected fingerprints in a file so
#    they can get further processed by e.g. the by-fp.py script.

fp_file="$1"
date=$(date +%s)
# Let's extract the fingerprints from the file. This is currently a check for
# RSA key ones, though.
fps=$(grep -oh -E "[0-9A-Fa-f]{40}" "$fp_file" | sort | uniq)
# Looping over the fingerprints: if we can't find them while checking the .conf
# files in our dirauth-conf repo, we should put them on the list to reject.
for fp in $fps;
do
  # Make sure we have the fingerprints upper-case.
  fp=${fp^^}
  checked_fp=$(find . -type f -name "*.conf" -exec grep "${fp}" {} +)
  if [ "$checked_fp" != "" ]
  then
    continue
  fi
  echo "${fp}" >> "missing_fps_$date"
done
