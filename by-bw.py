#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2018, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys, os
import stem

from datetime import datetime
from stem import Flag
from stem.descriptor import DocumentHandler, parse_file
from stem.descriptor.remote import DescriptorDownloader

from util import load_args_as_fp

# Fingerprints to test.
fps = []
total_bw = 0.0
guard_count = 0

START_BW = 50 * 1024
STEP_BW = 50 * 1024

# Load arguments and consider them as fingerprint which are put in fps.
#if len(sys.argv) < 2:
    #print("Missing first argument: maximum bandwidth")
    #sys.exit(1)
#target_max_bw = float(sys.argv[1])
target_max_bw = START_BW

# This will load all needed documents either from the cache in /tmp or
# download them from the dirauth.
from base import *

print("[+] Testing %f maximum bw" % (target_max_bw))

for bw in range(target_max_bw, target_max_bw * 11, STEP_BW):
    found = {}
    for fp, desc in rse.routers.items():
        sd = get_sd(desc)
        if sd is None:
            continue
        if float(sd.observed_bandwidth) > bw:
            continue
        found[fp] = desc

    print("%d bytes - %d" % (bw, len(found)))
