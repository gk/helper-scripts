#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2016, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys, os
import stem

from stem import Flag
from stem.descriptor import DocumentHandler, parse_file
from stem.descriptor.remote import DescriptorDownloader

DIR_PATH = "/tmp/tor-relay-docs"
EI_PATH = "%s/extra-infos" % (DIR_PATH)

extra_info = {}

if not os.path.isfile(EI_PATH):
    os.mkdir(DIR_PATH)
    downloader = DescriptorDownloader()
    e_infos = downloader.get_extrainfo_descriptors(\
            document_handler = DocumentHandler.DOCUMENT).run()
    for ei in e_infos:
        extra_info[ei.fingerprint] = ei
    with open(EI_PATH, "wb") as fd:
        ei_str = ''.join(map(str, e_infos))
        ei_str_out = ei_str.encode('utf-8')
        fd.write(ei_str_out)

# Load extra infos"
try:
	eis = parse_file(EI_PATH,
			descriptor_type = "extra-info 1.0",
			document_handler = DocumentHandler.ENTRIES)
except FileNotFoundError as fnfe:
	print("Extra info file not found")
	sys.exit(1)

for ei in eis:
	extra_info[ei.fingerprint] = ei

print("%d relays in extra infos." % (len(extra_info)))

list_seen = []
onions_seen = {}
for ei in extra_info.values():
    if ei.hs_dir_onions_seen is None:
        continue
    onions_seen[ei.fingerprint] = ei

print("%d have seen onions" % (len(onions_seen)))
sorted_by_seen = sorted(onions_seen, key = lambda ei: \
        onions_seen[ei].hs_dir_onions_seen, reverse=True)

count = 10
for ei in sorted_by_seen:
    if count == 0:
        break
    print(onions_seen[ei].hs_dir_onions_seen)
    count -= 1
