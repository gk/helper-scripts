# Copyright (c) 2019, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os, sys
import stem

from datetime import datetime, timedelta
from stem import Flag
from stem.descriptor import DocumentHandler, parse_file
from stem.descriptor.remote import DescriptorDownloader

DOC_PATH = "/tmp/tor-relay-docs"

CONSENSUS_FMT = "%s/consensus"
SD_FMT = "%s/server-descriptors"
BD_FMT = "%s/bridge-descriptors"

rse = None
sds = {}
bds = {}
downloader = DescriptorDownloader()

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def get_sd(desc):
    global sds
    try:
        return sds[desc.fingerprint]
    except KeyError as ke:
        #print("  (%s has no descriptor. Skipping" % (desc.fingerprint))
        pass
    return None

def describe(desc):
    global sds
    sd = get_sd(desc)
    print("  [+] %s" % (desc.fingerprint))
    # Print useful info.
    contact = ""
    if sd is not None and sd.contact is not None:
        contact = sd.contact.decode('utf-8')
    version = ""
    if sd is not None and sd.tor_version is not None:
        version = sd.tor_version
    print("      > Addr: %s - Contact: '%s' - Nickname: '%s' - Version: %s" % \
            (desc.address, contact, desc.nickname, version))
    print("      > Flags: %s" % (desc.flags))
    dir_port = 0
    if sd is not None and sd.dir_port is not None:
        dir_port = sd.dir_port
    or_port = 0
    if sd is not None and sd.or_port is not None:
        or_port = sd.or_port
    print("      > OR Port: %d, Dir Port: %d" % (or_port, dir_port))
    if sd is not None:
        print("      > Bandwidth: %s MB/s" % \
                str(sd.observed_bandwidth / 1000.0 / 1000.0))
        now = datetime.now()
        delta = now - (now - timedelta(seconds=sd.uptime))
        print("      > Uptime: %s" % (delta))
    print("      https://metrics.torproject.org/rs.html#details/%s" % \
            (desc.fingerprint))

def load_docs(path):
    global rse, sds, bds
    try:
        rse = next(parse_file(CONSENSUS_FMT % (path),
            descriptor_type = "network-status-consensus-3 1.0",
            document_handler = DocumentHandler.DOCUMENT,
        ))

        data = parse_file(SD_FMT % (path),
                descriptor_type = "server-descriptor 1.0",
                document_handler = DocumentHandler.ENTRIES)
        for sd in data:
            sds[sd.fingerprint] = sd

        data = parse_file(BD_FMT % (path),
                descriptor_type = "server-descriptor 1.0",
                document_handler = DocumentHandler.ENTRIES)
        for bd in data:
          bds[bd.fingerprint] = bd
    except FileNotFoundError as fnfe:
        print("%s not found")
        return

def download_docs(path):
    # Get Router status entries.
    data = downloader.get_consensus(\
            document_handler = DocumentHandler.DOCUMENT).run()[0]
    with open("%s/consensus" % (path), "w") as fd:
        fd.write(str(data))

    # Get server descriptors.
    data = downloader.get_server_descriptors(\
            document_handler = DocumentHandler.DOCUMENT).run()
    with open("%s/server-descriptors" % (path), "wb") as fd:
        sd_str = ''.join(map(str, data))
        sd_str_output = sd_str.encode('utf-8')
        fd.write(sd_str_output)

    # Get bridge descriptors.
    data = downloader.get_server_descriptors(\
            descriptor_type = "bridge-server-descriptor 1.0",
            document_handler = DocumentHandler.DOCUMENT).run()
    with open("%s/bridge-descriptors" % (path), "wb") as fd:
        sd_str = ''.join(map(str, data))
        sd_str_output = sd_str.encode('utf-8')
        fd.write(sd_str_output)

def init_docs(path=None):
    if path is None:
        path = DOC_PATH
    # Load latest data from temporary path.
    if not os.path.isdir(path):
        os.mkdir(path)
        download_docs(path)
    else:
        if not os.path.isfile(CONSENSUS_FMT % path) or \
           not os.path.isfile(SD_FMT % path) or \
           not os.path.isfile(BD_FMT % path):
            download_docs(path)
        else:
            # Check if files are too old that is we should get the latest docs.
            ts = os.path.getmtime(CONSENSUS_FMT % (path))
            delta = datetime.now() - datetime.fromtimestamp(ts)
            # We have a file that is at least an hour old, download all the things.
            if delta.seconds >= 3600:
                download_docs(path)
    load_docs(path)
    print("[+] Tor documents loaded successfully")
    print("      - %d relays in consensus" % (len(rse.routers)))
    print("      - %d server descriptors" % (len(sds)))
    print("      - %d bridge descriptors" % (len(bds)))

# Load everyting when this thing is loaded.
init_docs()
